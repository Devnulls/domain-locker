/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package crypto;


import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.util.Base64;

public class AesCipher1 {

    private static String CIPHER_NAME = "AES/CBC/PKCS5PADDING";
    private static int CIPHER_KEY_LEN = 16; //128 bits
    /**
     * Encoded/Decoded data
     */
    protected String data;

    public AesCipher1() {
    }

    public AesCipher1(String data) {
        this.data = data;
    }
    
    

    /**
     * Encrypt data using AES Cipher (CBC) with 128 bit key
     * 
     * 
     * @param key  - key to use should be 16 bytes long (128 bits)
     * @param iv - initialization vector
     * @param data - data to encrypt
     * @return encryptedData data in base64 encoding with iv attached at end after a :
     */
    public static AesCipher1 encrypt(String key, String data) {
        String iv = "fedcba9876543210"; // 16 bytes IV
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < key.length(); i += 2) {
            String str = key.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }
        key = output.toString();
        try {
            if (key.length() < AesCipher1.CIPHER_KEY_LEN) {
                int numPad = AesCipher1.CIPHER_KEY_LEN - key.length();
                
                for(int i = 0; i < numPad; i++){
                    key += "0"; //0 pad to len 16 bytes
                }
                
            } else if (key.length() > AesCipher1.CIPHER_KEY_LEN) {
                key = key.substring(0, CIPHER_KEY_LEN); //truncate to 16 bytes
            }
            
            
            IvParameterSpec initVector = new IvParameterSpec(iv.getBytes("ISO-8859-1"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("ISO-8859-1"), "AES");

            Cipher cipher = Cipher.getInstance(AesCipher1.CIPHER_NAME);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, initVector);

            byte[] encryptedData = cipher.doFinal((data.getBytes()));
            
            String base64_EncryptedData = Base64.getEncoder().encodeToString(encryptedData);
            String base64_IV = Base64.getEncoder().encodeToString(iv.getBytes("ISO-8859-1"));
            
            //return base64_EncryptedData + ":" + base64_IV;
            return new AesCipher1(base64_EncryptedData + ":" + base64_IV);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //return null;
        return new AesCipher1(null);
    }

    /**
     * Decrypt data using AES Cipher (CBC) with 128 bit key
     * 
     * @param key - key to use should be 16 bytes long (128 bits)
     * @param data - encrypted data with iv at the end separate by :
     * @return decrypted data string
     */
    
    public static AesCipher1 decrypt(String key, String data) {
        try {
            if (key.length() < AesCipher1.CIPHER_KEY_LEN) {
                int numPad = AesCipher1.CIPHER_KEY_LEN - key.length();
                
                for(int i = 0; i < numPad; i++){
                    key += "0"; //0 pad to len 16 bytes
                }
                
            } else if (key.length() > AesCipher1.CIPHER_KEY_LEN) {
                key = key.substring(0, CIPHER_KEY_LEN); //truncate to 16 bytes
            }
            
            String[] parts = data.split(":");
            
            IvParameterSpec iv = new IvParameterSpec(Base64.getDecoder().decode(parts[1]));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("ISO-8859-1"), "AES");

            Cipher cipher = Cipher.getInstance(AesCipher1.CIPHER_NAME);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] decodedEncryptedData = Base64.getDecoder().decode(parts[0]);

            byte[] original = cipher.doFinal(decodedEncryptedData);

            //return new String(original);
            return new AesCipher1(new String(original));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new AesCipher1(null);
    }
    
    /**
     * Get encoded/decoded data
     */
    public String getData() {
        return data;
    }
    
    /**
     * To string return resulting data
     *
     * @return Encoded/decoded data
     */
    public String toString() {
        return getData();
    }
    
    
    public static void main(String[] args) {

        String key = "0123456789abcdef"; // 128 bit key
        

        System.out.println(decrypt(key,
                encrypt(key, "Hi").toString()));
        
         System.out.println(decrypt(key, "Kx+c85B7tWjre4j6cdXHJQ==:ZmVkY2JhOTg3NjU0MzIxMA==")); // output from PHP code Testing
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            

}
